#!/bin/bash

# format of input USERNAME [RIGHTS] DATABASE
# for example my_user [DELETE, READ, WRITE, CREATE] my_db

# !Comment this if you want to call this script with your own REQUEST value

REQUEST="my_user [SELECT, DELETE, TRIGGER, SHOW VIEW] my_db
my_user_2 [SELECT, DELETE, TRIGGER, SHOW VIEW] my_db_2"

# !IMPORTANT! Acceptables rights: ALL, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY
# TABLES, CREATE VIEW, DELETE, DROP, EVENT, EXECUTE, GRANT OPTION, INDEX, INSERT,
# LOCK TABLES, REFERENCES, SELECT, SHOW VIEW, TRIGGER, UPDATE

#Script settings
DEFAULT_PASSWORD="2zKsvZZT!!"
USE_DEFAULT_PASSWORD=1
PASSWORD_LENGTH=16
USE_UTF_8_CHARSET=1

#System variables. !!!DO NOT MODIFY!!!
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
COMMAND_COUNTER=0
SUCCESS_COUNTER=0
UNSUCCESS_COUNTER=0

while IFS= read -r line; do
#  echo $line
  RIGHTS="$(echo "$line" | awk -F'[][]' '{print $2}' |  sed 's/[][]//g')"
  USERNAME="$(echo "$line" | sed -e 's/\[[^][]*\]//g' |cut -d' ' -f1)"
  DATABASE="$(echo "$line" | sed -e 's/\[[^][]*\]//g' |cut -d' ' -f3)"
  echo "PARSING REQUEST:$USERNAME [$RIGHTS] $DATABASE"
  if [ $(sudo mysql --defaults-extra-file=./config.cnf -e "SELECT COUNT(*) FROM mysql.user WHERE user = '$USERNAME'" | tail -n1) -gt 0 ]
  then
    echo "USER $USERNAME ALREADY EXITST"
  else
    if [ USE_DEFAULT_PASSWORD -gt 0 ]
    then
      echo "CREATING USER $USERNAME WITH DEFAULT PASSWORD $DEFAULT_PASSWORD"
      sudo mysql --defaults-extra-file=./config.cnf -e "CREATE USER ${USERNAME}@localhost IDENTIFIED BY '${DEFAULT_PASSWORD}';"
    else
      PASSWORD=$(openssl rand -base64 $PASSWORD_LENGTH)
      echo "CREATING USER $USERNAME WITH PASSWORD $PASSWORD"
      sudo mysql --defaults-extra-file=./config.cnf -e "CREATE USER ${USERNAME}@localhost IDENTIFIED BY '${PASSWORD}';"
    fi
  fi
  if [ $(sudo mysql --defaults-extra-file=./config.cnf -e "SELECT COUNT(*) FROM information_schema.schemata WHERE schema_name = '$DATABASE'" | tail -n1) -gt 0 ]
  then
    echo "DATABASE $DATABASE ALREADY EXISTS"
  else
    echo "CREATING DATABASE $DATABASE WITH CHARSET UTF-8"
    if [ USE_UTF_8_CHARSET -gt 0 ]
    then
      sudo mysql --defaults-extra-file=./config.cnf -e "CREATE DATABASE ${DATABASE} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
    else
      sudo mysql --defaults-extra-file=./config.cnf -e "CREATE DATABASE ${DATABASE};"
    fi
  fi
  echo "GIVING THE PERMISSIONS [$RIGHTS] TO USER $USERNAME FOR DATABASE $DATABASE"
  sudo mysql --defaults-extra-file=./config.cnf -e "GRANT ${RIGHTS} ON ${DATABASE}.* TO ${USERNAME}@localhost"
  if [ $? -eq 0 ]
  then
    ((SUCCESS_COUNTER++))
  else
    ((UNSUCCESS_COUNTER++))
    echo "${red}COMMAND FAILED: GRANT ${RIGHTS} ON ${DATABASE}.* TO ${USERNAME}@localhost${reset}"
  fi
  ((COMMAND_COUNTER++))
done < <(printf '%s\n' "$REQUEST")
echo "PARSED $COMMAND_COUNTER OPERATIONS: ${green}$SUCCESS_COUNTER${reset} SUCCESS; ${red}$UNSUCCESS_COUNTER${reset} FAILED"
